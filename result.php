<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width,user-scalable=no"/>
	<title>Tim Cook Game</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
    <form action="game.html" method="get" id="content" class="controls">

        <div class="row">
        	<div class="col-md-4"></div>
            <div class="col-md-4">

            	<h2>GAME OVER</h2>

            	<h3>Вы набрали <?=$_GET['score']?></h3>

				<ul class="social-circle-list">
					<li><a href="#">Facebook<i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#">VK<i class="fa fa-vk" aria-hidden="true"></i></a></li>
				</ul>           	

				<a href="#" onclick="window.history.back();" class="btn btn-primary">Играть заново</a>
	
            </div>
            <div class="col-md-4"></div>
        </div>
    </form>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="/js/social.js"></script>
</body>
</html>
