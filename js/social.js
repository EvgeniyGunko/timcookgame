var social = {
    networks: {
        vk: {
            class: 'fa-vk',
            link: 'https://vk.com/share.php?url='
        },

        facebook: {
            class: 'fa-facebook',
            link: 'https://www.facebook.com/sharer/sharer.php?u='
        },

        ok: {
            class: 'fa-odnoklassniki',
            link: 'https://connect.ok.ru/offer?url='
        },

        twitter: {
            class: 'fa-twitter',
            link: 'https://twitter.com/intent/tweet?url='
        }
    },

    windowParams: {
        toolbar: 0,
        status: 0,
        width: 626,
        height: 636
    },
    windowName: 'social_share',

    containerSelector: 'ul.social-circle-list',
    elementSelector: 'li > a:not(.ulogin-button)',

    classQuickView: 'social-list-quick-view',
    linkQuickView: 'data-link',


    init: function() {
        var env = this;
        var body = $('body');

        body.on('click', this.containerSelector + ' > ' + this.elementSelector, function (event) {
            event.preventDefault();
            env.action($(this).children());
        });
    },


    action: function (target) {
        var network = this.getTargetNetwork(target);
        var link = this.getTargetLink(target);
        if(!network)
            return;


        window.open(network.link + link, this.windowName, this.getWindowParams());
    },

    getTargetNetwork: function (target) {
        for(var name in this.networks) {
            if(!this.networks.hasOwnProperty(name))
                continue;

            if(target.hasClass(this.networks[name].class))
                return this.networks[name];
        }


        return false;
    },

    getTargetLink: function (target) {
        var container = target.parents(this.containerSelector);
        if(container.length) {
            return encodeURIComponent(container.hasClass(this.classQuickView) ? window.location.origin + container.attr(this.linkQuickView) : window.location.href);
        }


        return false;
    },

    getWindowParams: function () {
        var params = [];

        for(var name in this.windowParams) {
            if(!this.windowParams.hasOwnProperty(name))
                continue;

            params.push(name + '=' + this.windowParams[name]);
        }


        return params.join(',');
    }
};

$(document).ready(function(){
    social.init();
});