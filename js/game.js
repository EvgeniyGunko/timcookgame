var pjs = new PointJS('2D', 720 / 2, 720 / 2, { // 16:9
	backgroundColor : 'background: linear-gradient(45deg, red, blue)'
});
pjs.system.initFullPage(); // for Full Page mode

var imagePath = 'img/';
var log    = pjs.system.log;     // log = console.log;
var game   = pjs.game;           // Game Manager
var point  = pjs.vector.point;   // Constructor for Point
var camera = pjs.camera;         // Camera Manager
var brush  = pjs.brush;          // Brush, used for simple drawing
var OOP    = pjs.OOP;            // Object's manager
var math   = pjs.math;           // More Math-methods
var levels = pjs.levels;         // Levels manager

var key   = pjs.keyControl.initKeyControl();
//var touch = pjs.mouseControl.initMouseControl();
var touch = pjs.touchControl.initTouchControl();

var width  = game.getWH().w; // width of scene viewport
var height = game.getWH().h; // height of scene viewport
// Получим резолюцию экрана
var r = game.getResolution();


pjs.system.setTitle('Tim Cook game'); // Set Title for Tab or Window

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

function getRandomObjectPath(objectType) {
	if (objectType) 
		return imagePath + 'good-objects/' + getRndInteger(1, 27) + '.png'
	else 
		return imagePath + 'bad-objects/' + getRndInteger(1, 7) + '.png'
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function sendScore(score) {
	var name = getUrlParameter("name");
	var phone = getUrlParameter("phone");
	$.post( "php/saveScore.php", { name: name, phone: phone, score: score }, 
		function(data){
			location.href = "/result.php?score=" + score;
		}
	);
}

game.newLoopFromConstructor('myGame', function () {

	// Объявим переменную скорости
	var speed = 2;
	var speedMax = 5;
	var giftNumberMax = 2;

	// Объявим переменну счета
	var score = 0;

	// Первым делом создадим фон

	var back = game.newImageObject({
		file : imagePath + 'background.jpg',
		h : height * r * 1.2 // Растягивание фона под экран
	});

	// Теперь создадим деда мороза (ну или санту)
	var player = game.newImageObject({
		file : imagePath + 'tim.png',
		h : 200 * r, // Оптимальный размер санты
		onload : function () {
			// отпозиционируем его по высоте
			this.y = -this.h + height + 10*r; // Отлично
		}
	});

	// Объявим массив с подарками
	var objects = [];

	var giftInterval = 2000;
	// Создадим таймер, который будет добавлять подарки
	var timer = OOP.newTimer(giftInterval, function () {

		for (var i = 1; i <= giftNumberMax; i++) {
			var objectType = getRndInteger(0, 1);
			objects.push(game.newImageObject({
				x : math.random(0, width - 50*r), // 50*r - ширина объекта
				y : -math.random(50*r, 500*r), // уберем минус, так как он уже есть
				w : 50*r, h : 50*r,
				isBad : objectType,
				file : getRandomObjectPath(objectType)
			}));
		}
	});

	this.update = function () {

		// Задействуем фактор дельта-тайм
		var dt = game.getDT(10); // 10 - это делитель дкльты для
		// удобного округления

		game.clear(); // clear screen

		back.draw(); // Отрисуем фон
		player.draw(); // Отрисуем санту

		// Алгоритм добавления подарков по таймеру
		// новый подарок каждую секунду

		// Для того, чтобы подарки добавлялись каждую секунду
		timer.restart();
	
		OOP.forArr(objects, function (el, i) { // i - идентификатор
			el.draw(); // Рисуем подарок

			el.move(point(0, speed*dt)); // Двигаем вниз

			// Проверка на столкновение подарка с сантой

			if (el.isIntersect(player)) {
				if (el.file.indexOf('bad') > -1) {
					game.stop();
					sendScore(score);
					brush.drawText({
						x : 10, y : 100,
						text : 'GAME OVER',
						size : 50 * r,
						color : '#FFFFFF',
						strokeColor : 'black',
						strokeWidth : 2,
						style : 'bold',
						font : 'Arial'
					});
				}

				objects.splice(i, 1); // i - идентификатор, 1 - количество
				score++; // Увеличиваем счет
				if (speed < speedMax)
					speed+= 0.01; // увеличиваем скорость
			}

		});

		// Заставим двигатьcz санту
		// Учтем ограничения движения

		if (key.isDown('LEFT')) {
			// Двигаем влево
			if (player.x >= 0)
				player.x -= speed * 4 * dt;
		}

		if (key.isDown('RIGHT')) {
			// Двигаем вправо
			if (player.x+player.w < width)
				player.x += speed * 4 * dt;
		}

		var touchPoint = touch.getPosition();
		//console.log(touchPoint);


		if (touchPoint && touchPoint.x && touchPoint.y) {
			if (touchPoint.x > width / 2 && player.x + player.w <= width)
				player.x += speed * dt;
			if (touchPoint.x < width / 2 && player.x >= 0)
				player.x -= speed * dt;

		}

		// Отрисуем счет
		brush.drawText({
			x : 10, y : 10,
			text : 'Счет: ' + score,
			size : 50 * r,
			color : '#FFFFFF',
			strokeColor : 'black',
			strokeWidth : 2,
			style : 'bold',
			font : 'Arial'
		});

	};

	this.entry = function () { // [optional]
		// При входе в игру будем очищать подарки и удалять счет
		OOP.clearArr(objects);
		score = 0;
	};

});

game.startLoop('myGame');